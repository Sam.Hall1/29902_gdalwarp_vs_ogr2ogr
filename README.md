# 29902 gdalwarp vs ogr2ogr

This repository uses docker compose to generate outputs from ogr2ogr and gdalwarp
using gdal docker images.

To generate outputs use `docker compose up` from the root of the repository.
